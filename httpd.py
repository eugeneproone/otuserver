import socket
import signal
import re
import select
import queue
import logging

from urllib.parse import unquote
from pathlib import Path
from time import sleep, strftime, gmtime
from multiprocessing import Process, Queue
from argparse import ArgumentParser

CONTENT_DELIM = '\r\n\r\n'

class Request:
    def __init__(self, content : bytes):
        self._text = content.decode('ascii')
        logging.debug("Request received: %s", self._text)
        header_ends_at = self._text.find(CONTENT_DELIM)
        if header_ends_at < 0:
            header_ends_at = len(self._text)
        self.fields = {}
        for line in self._text[:header_ends_at].split('\r\n'):
            prop_match = re.match('^[\w-]+:', line)
            if prop_match != None:
                prop = prop_match[0][:-1]
                self.fields[prop] = line[prop_match.end() + 1:].strip()
        req_type_match = re.match(r'^\w+', self._text)
        self.req_type = req_type_match[0].lower() if req_type_match != None else "unknown"
        try:
            self.req_filepath = re.search(r'\w+\s(\S+)', self._text)[1]
        except:
            self.req_filepath = None

class Response:
    KNOWN_TEXT_EXTENSIONS = {
        '.js' : 'application/javascript',
        '.html' : 'text/html',
        '.css' : 'text/css',
        '.jpg' : 'image/jpeg',
        '.jpeg' : 'image/jpeg',
        '.png' : 'image/png',
        '.gif' : 'image/gif',
        '.swf' : 'application/x-shockwave-flash'
    }

    KNOWN_REQ_TYPES = ('get', 'head')
    
    def __init__(self, request : Request, document_root, server_name: str):
        self.server = server_name
        self.request = request
        self.document_root = document_root

    def send(self, conn):
        if (self.request.req_type in Response.KNOWN_REQ_TYPES):
            content = { 'type' : None, 'data' : None }
            file_found = self.request.req_filepath != None
            if file_found:
                target_filepath = unquote(str(self.document_root) + self.request.req_filepath)
                params_begin_at = target_filepath.find('?')
                if params_begin_at >= 0:
                    target_filepath = target_filepath[:params_begin_at]
                target_filepath = Path(target_filepath)
                if self.request.req_filepath.endswith('/'):
                    target_filepath /= 'index.html'
                logging.info("target_filepath: %s", target_filepath)

                target_filepath_parts = target_filepath.resolve().parts
                for part in self.document_root.parts:
                    if part not in target_filepath_parts:
                        file_found = False
                        break
                else:
                    file_found = target_filepath.exists()
                if file_found:
                    logging.info('target_filepath: %s' % str(target_filepath))
                    for k, v in Response.KNOWN_TEXT_EXTENSIONS.items():
                        if str(self.request.req_filepath).endswith(k):
                            content['type'] = v
                            break
                    with open(target_filepath, 'rb') as file:
                        content['data'] = file.read()
                    start_line = "HTTP/1.1 200 OK"
            if not file_found:
                start_line = "HTTP/1.1 404 Not_Found"
            conn.send((start_line
                    + '\r\nServer: ' + self.server
                    + (('\r\nContent-Type: ' + content['type']) if content['type'] != None else '')
                    + '\r\nDate: ' + strftime("%a, %d %b %Y %H:%M:%S GMT", gmtime())
                    + '\r\nContent-Length: ' + str(len(content['data']) if content['data'] != None else 0)
                    + '\r\nConnection: close'
                    + CONTENT_DELIM
                    ).encode('utf-8'))
            if (content['data'] != None) and (self.request.req_type == 'get'):
                conn.send(content['data'])
        else:
            conn.sendall(("HTTP/1.1 405 Method_Not_Allowed"
                    + '\r\nServer: ' + self.server
                    + '\r\nDate: ' + strftime("%a, %d %b %Y %H:%M:%S GMT", gmtime())
                    + '\r\nContent-Length: 0'
                    + '\r\nConnection: close'
                    + CONTENT_DELIM
                    ).encode('utf-8'))
            

def _worker(id, document_root, q : Queue):
    try:
        conn = None
        while True:
            try:
                conn, addr = q.get(block=False)
            except queue.Empty:
                pass
            else:
                logging.info("id %u accepted connection: %s", id, addr)
                conn.setblocking(0)
                try:
                    rlist, wlist, xlist = select.select([conn], [], [], .1)
                except:
                    pass
                else:
                    if conn in rlist:
                        logging.info("Connection appeared in worker")
                        received_bytes = bytearray()
                        while True:
                            try:
                                cur_rcvd = conn.recv(1024)
                            except BlockingIOError:
                                logging.info("Received %u bytes" % len(received_bytes))
                                req = Request(received_bytes)
                                resp = Response(req, document_root, 'Otuserver')
                                resp.send(conn)
                                conn.close()
                                break
                            except (ConnectionAbortedError, BrokenPipeError):
                                logging.exception("Error happened while receiving data")
                                conn.close()
                                break
                            else:
                                received_bytes += cur_rcvd
    except KeyboardInterrupt:
        if conn != None:
            conn.close()

class Otuserver:
    def __init__(self, document_root : str, bind_addr: tuple[str, int], backlog=10, workers_qty=1):
        self.document_root = Path(document_root)
        self.bind_addr = bind_addr
        self.backlog = backlog
        self.workers_qty = workers_qty
        self.workers_list = None
        self.stop = False

    def _sigint_handler(self, signum, frame):
        logging.exception('CTRL+C received. Stopping server')
        if self.workers_list is not None:
            for proc in self.workers_list:
                proc.terminate()
        self.stop = True

    def serve_forever(self):
        self.stop = False
        signal.signal(signal.SIGINT, self._sigint_handler)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            queue = Queue(100)
            self.workers_list = list(Process(target=_worker, args=(i, self.document_root, queue), name="Worker%u" % i) for i in range(self.workers_qty))
            for proc in self.workers_list:
                proc.start()
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind(self.bind_addr)
            sock.listen(self.backlog)
            sock.setblocking(0)
            while not self.stop:
                rlist, wlist, xlist = select.select([sock],  [], [], .1)
                for sock in rlist:
                    queue.put(sock.accept())
            logging.warning("Finished serving")


if __name__ == '__main__':
    arg_parser = ArgumentParser(description="Yet another tiny web server")
    arg_parser.add_argument('-r', '--document-root', nargs=1, help="Document root folder", dest='document_root', required=True)
    arg_parser.add_argument('-w', '--workers', nargs=1, help="Workers qty", dest='workers_qty', default='1')
    arg_parser.add_argument('-p', '--port', nargs=1, help="HTTP-sever port", dest='port', default='80')
    arg_parser.add_argument('-b', '--backlog', nargs=1, help="Backlog value", dest='backlog', default='10')
    arg_parser.add_argument('--log-file', nargs=1, help="Logging filepath", dest='log_filepath', default=None)
    arg_parser.add_argument('--log-level', nargs=1, help="Logging level according to logging module", dest='log_level', default=["WARNING"])
    args = arg_parser.parse_args()

    log_filepath = Path(args.log_filepath[0].strip()) if args.log_filepath else None
    logging.basicConfig(level=args.log_level[0].strip(), filename=log_filepath)
    port = int(args.port[0])
    document_root = Path(args.document_root[0].strip())
    workers_qty = int(args.workers_qty[0])
    backlog = int(args.backlog[0])
    serv = Otuserver(document_root, ('0.0.0.0', port), backlog=backlog, workers_qty=workers_qty)
    serv.serve_forever()